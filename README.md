# redditcheck
Lightweight extension checks if the current page has been submitted to reddit.

https://addons.mozilla.org/en-US/firefox/addon/redditcheck/