const ENDPOINT = 'https://reddit.com/api/info.json?url='

const ROOT_CLASS = 'main'

function isValidHttpUrl(string) {
    try {
        let url = new URL(string);
        return url.protocol === "http:" || url.protocol === "https:";
    } catch (_) {
        return false;  
    }
}

async function getSubmissions(url) {
    let api_call = ENDPOINT + url;
    let response = await fetch(api_call);
    return response.json();
}

/*
    <div class="entry">
        <div class="title">
            <a href=https://reddit.com${element.data.permalink}>${element.data.title}</a>
        </div>
        <div class="subtext">
            <span class="score">${element.data.score}⬆️</span>
            <span class="comments">${element.data.num_comments}💬</span>
            <span class="origin">r/${element.data.subreddit}</span>
            <span class="user">u/${element.data.author_fullname}</span>
        </div>
    </div>
*/
function populateEntryNode(submission, parentElement) {
    let entry = document.createElement("div")
    entry.className = "entry"

    let title = document.createElement("div")
    title.className = "title"
    let link = document.createElement("a")
    link.textContent = submission.data.title
    link.setAttribute("href", 'https://reddit.com' + submission.data.permalink)
    title.appendChild(link)
    entry.appendChild(title)

    let subtext = document.createElement("div")
    subtext.className = "subtext"
    let score = document.createElement("span")
    score.className = "score"
    score.textContent = submission.data.score + "⬆️"
    subtext.appendChild(score)
    let comments = document.createElement("span")
    comments.className = "comments"
    comments.textContent = submission.data.num_comments + "💬"
    subtext.appendChild(comments)
    let origin = document.createElement("span")
    origin.className = "origin"
    origin.textContent = submission.data.subreddit_name_prefixed
    subtext.appendChild(origin)
    let user = document.createElement("span")
    user.className = "user"
    user.textContent = 'u/' + submission.data.author
    subtext.appendChild(user)
    entry.appendChild(subtext)

    parentElement.appendChild(entry)
}

function generateHTMLNodes(url, tag) {
    let root_element = document.getElementById(tag)
    root_element.textContent = ''

    if (isValidHttpUrl(url)) {
        /*
            <div class="submit">
                <a href=https://reddit.com/submit?resubmit=true&url=${url}>
                    submit
                </a>
            </div>
        */
        let submit = document.createElement("div")
        submit.className = "submit"
        let submission_link = document.createElement("a")
        submission_link.textContent = "submit"
        submission_link.setAttribute("href", "https://reddit.com/submit?resubmit=true&url=" + url)
        submit.appendChild(submission_link)
        root_element.appendChild(submit)
        
        getSubmissions(url).then(json => {
            if (json.data.children && json.data.children.length) {
                let entries = document.createElement("div")
                entries.className = "entries"
                for (let child of json.data.children) {
                    populateEntryNode(child, entries)
                }
                root_element.appendChild(entries)
            } else {
                /*
                    <div class="noentry">
                        <span class="none">no submissions</span>
                    </div>
                */
                let noentry = document.createElement("div")
                noentry.className = "noentry"
                let none = document.createElement("span")
                none.className = "none"
                none.textContent = "no submissions"
                noentry.appendChild(none)
                root_element.appendChild(noentry)
            }
        }, reason => {
            console.log("Unable to retrieve submissions from reddit. " + reason)
            let blocked = document.createElement("div")
            blocked.className = "blocked"
            let error = document.createElement("span")
            error.className = "error"
            error.textContent = "unable to retrieve submissions from reddit (disable enhanced tracking protection?)"
            blocked.appendChild(error)
            root_element.appendChild(blocked)
        })
    } else {
        /*
            <div class="nopage">
                <span class="none">navigate to a webpage</span>
            </div>
        */
        let nopage = document.createElement("div")
        nopage.className = "nopage"
        let none = document.createElement("span")
        none.textContent = "navigate to a webpage"
        nopage.appendChild(none)
        root_element.appendChild(nopage)
    }
}

browser.tabs.query({currentWindow: true, active: true}).then(tabs => {
    if (tabs.length != 1) {
        console.log("Unable to retrieve current tab.")
    } else {
        let url = tabs[0].url
        generateHTMLNodes(url, ROOT_CLASS)
    }
}, reason => {
    console.log("Unable to retrieve tabs." + reason)
})